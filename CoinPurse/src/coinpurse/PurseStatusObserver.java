package coinpurse;

import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
/**
 * Observer for updating status and show progress bar.
 * @author Prin Angkunanuwat
 *
 */
public class PurseStatusObserver extends JFrame implements Observer {
	
	private JLabel status;
	private JProgressBar progress;
	/**
	 * Set title and call initComponent.
	 */
	public PurseStatusObserver(){
 		super("Purse Status Observer");
		
		this.initComponents();
	}
	/**
	 * Create component used in this frame.
	 */
	private void initComponents() {
		JPanel pane = new JPanel();
		
		status = new JLabel("Empty");
		progress = new JProgressBar(0,10);
		pane.setLayout(new GridLayout(2,1));
		pane.add(status);
		pane.add(progress);
		super.add(pane);
		super.pack();
		super.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * Whenever purse's content is changed, this gui will change its value.
	 * @param subject an Observable that is changed.
	 * @param info is not used.
	 */
	@Override
	public void update(Observable subject, Object info) {
		// TODO Auto-generated method stub
		if (subject instanceof Purse) {
			Purse purse = (Purse) subject;
			int count = purse.count();
			int max = purse.getCapacity();
			progress.setMaximum(max);
			progress.setValue(count);
			if(count == 0) status.setText("Empty  ");
			if(count > 0 && count < max) status.setText(count + "  ");
			if(count == max) status.setText("Full  ");
			super.pack();
		}
		if (info != null) System.out.println( info );

		
	}
	
	
}
