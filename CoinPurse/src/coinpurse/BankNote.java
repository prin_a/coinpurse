package coinpurse;
/**
 * A Banknote with value and serial number.
 * @author Prin Angkunanuwat
 *
 */
public class BankNote extends AbstractValuable {
	private long serialNumber;
	private static long nextSerialNumber = 1_000_000;
	/**
	 * Constructor of banknote.
	 * @param value of the banknote.
	 */
	public BankNote(double value,String currency){
		super(value, currency);
		serialNumber = getNextSerialNumber();
	}
	/**
	 * Make unique serialnumber.
	 * @return serialnumber and add 1 to it.
	 */
	public static long getNextSerialNumber(){
		return nextSerialNumber++;
	}
	
	
	/**
	 * describe Banknote.
	 * @return String of banknote.
	 */
	public String toString(){
		return String.format("%.0f %s Banknote", this.getValue(), this.getCurrency());
	}
}
