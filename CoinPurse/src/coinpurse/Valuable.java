package coinpurse;
/**
 * An interface used to get the value.
 * @author Prin Angkunanuwat
 *
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * Method use to get value.
	 * @return value of an object.
	 */
	public double getValue( );
}
