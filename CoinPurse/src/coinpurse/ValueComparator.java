package coinpurse;

import java.util.Comparator;
/**
 * interface used to compare the value.
 * @author Prin Angkunanuwat
 *
 */
public class ValueComparator implements Comparator<Valuable> {
	/**
	 * Compare method use to compare the value.
	 * @param a is the first object.
	 * @param b is the second object.
	 * @return -1 if a has lower value ,1 if a has higher value and 0 if the values are same.
	 */
	public int compare(Valuable a, Valuable b) {
		if(a.getValue() > b.getValue()) return 1;
		if(a.getValue() < b.getValue()) return -1;
		return 0;
	}
}
