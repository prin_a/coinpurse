package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
/**
 * Withdraw using recursive.
 * @author Prin Angkunanuwat
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy{
	private double amount;
	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		this.amount = amount;
		List temp = withdrawFrom(amount , valuables, valuables.size()-1);
		
		if(temp == null || this.amount != 0) return null;

		Valuable [] array = new Valuable[temp.size()];
		temp.toArray(array);
		return array;
	}
	private List<Valuable> withdrawFrom(double remainAmount, List<Valuable> valuables,int index){
		if(remainAmount < 0 || (index < 0 && remainAmount > 0) ) return null;
		
		
		List<Valuable> temp = new ArrayList<Valuable>();
		if(remainAmount == 0) return temp;
			
		
		double subtractAmount = remainAmount - valuables.get(index).getValue();
	
		if(subtractAmount < 0) return withdrawFrom(remainAmount,valuables,index - 1);

		List<Valuable> temp2 = withdrawFrom(subtractAmount,valuables,index - 1);
		if( temp2 != null) temp.addAll(temp2);
		temp.add(0,valuables.get(index));
		amount -= valuables.get(index).getValue();
		
		return temp;
		
		

	}

}
