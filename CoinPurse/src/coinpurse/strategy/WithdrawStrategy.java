package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * An interface has withdraw method.
 * @author Prin Angkunanuwat
 *
 */
public interface WithdrawStrategy {
	/**
	 * Withdraw method use to get an array of valuable that purse will withdraw.
	 * @param amount is value of money want to withdraw.
	 * @param valuables is list of valuables
	 * @return Array of valuable that will be withdraw in the purse.
	 */
    Valuable[] withdraw (double amount, List<Valuable> valuables);
}
