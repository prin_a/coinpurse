package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
/**
 * GreedyWithdraw is to withdraw with out strategy.
 * @author Prin Angkunanuwat
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {

	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		
		ArrayList <Valuable>templist = new ArrayList<Valuable>();

		for(int i = valuables.size()-1; i >= 0;i--){
			if(amount - valuables.get(i).getValue() >= 0){
				templist.add(valuables.get(i));
				amount -= valuables.get(i).getValue();
			}
		}

		Valuable [] array = new Valuable[templist.size()];
		templist.toArray(array);
		
		for(int i = 0 ; i < array.length ; i++){ System.out.println(array[i]);}
		
		if ( amount > 0)
		{
			return null;
		}

		return array;
	}
	
}
