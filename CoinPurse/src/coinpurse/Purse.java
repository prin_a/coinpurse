package coinpurse;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.WithdrawStrategy;
/**
 *  A purse contains Valuable.
 *  You can insert Valuable, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the purse decides which
 *  Valuable to remove.
 *  
 *  @author Prin Angkunanuwat
 */
public class Purse extends Observable {
	/** Collection of Valuable in the purse. */
	private List <Valuable>valuables;
	private WithdrawStrategy strategy;
	/** Capacity is maximum NUMBER of Valuable the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;
	private final ValueComparator comparator = new ValueComparator();

	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of Valuable you can put in purse.
	 */
	public Purse( int capacity ) {
		this.capacity = capacity;
		valuables = new ArrayList<Valuable>();
	}

	/**
	 * Count and return the number of Valuable in the purse.
	 * This is the number of Valuable, not their value.
	 * @return the number of Valuable in the purse
	 */
	public int count() { return valuables.size(); }

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double balance = 0;
		for(int i = 0;i < count();i++){
			balance += valuables.get(i).getValue();
		}
		return balance;
	}


	/**
	 * Return the capacity of the purse.
	 * @return the capacity
	 */

	public int getCapacity() { return this.capacity; }

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		return count() == getCapacity();
	}

	/** 
	 * Insert a valuable into the purse.
	 * The valuable is only inserted if the purse has space for it
	 * and the valuable has positive value.  No worthless valuable!
	 * @param valuable is a valuable object to insert into purse
	 * @return true if valuable inserted, false if can't insert
	 */
	public boolean insert( Valuable valuable ) {

		if(isFull()){
			return false;
		} else if(valuable.getValue() <= 0) {
			return false;
		} else { 
			valuables.add(valuable);
			super.setChanged();
			super.notifyObservers( this );
			return true;
		}
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of valuable withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		
		if(amount <= 0) return null;
		Collections.sort(valuables, comparator);
		
		Valuable [] array = strategy.withdraw( amount, valuables );
		if ( array == null ) return null;	
		else {
			int arrayIndex = array.length - 1;
			for(int i = 0;i < count();i++){
				for(int j = arrayIndex;j >= 0; j--){
					if(valuables.get(i).getValue() == array[j].getValue()){
						valuables.remove(i);
						arrayIndex = j-1;
						i--;
						break;
					}
				}
			}
			super.setChanged();
			super.notifyObservers( this );
			return array;
		}
	}
	
	/**
	 * Set current strategy to be new strategy.
	 * @param strategy is new strategy.
	 */
	public void setWithdrawStrategy( WithdrawStrategy strategy ){
		this.strategy = strategy;
	}

	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 * @return Describe Purse.
	 */
	public String toString() {
		return valuables.size()+ " valuables with value " + getBalance();
	}

}