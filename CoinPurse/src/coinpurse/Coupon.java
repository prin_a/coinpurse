package coinpurse;
/**
 * Coupon has color.
 * Each color has different value.
 * @author Prin Angkunanuwat
 *
 */     
public class Coupon extends AbstractValuable {
	private String color;
	/**
	 * enum use to specific value for each color.
	 */
	public enum  CouponColor{
		RED(100) , BLUE(50)  , GREEN(20) ;
		private double value;

		private CouponColor(double value) {
			this.value = value;
		}
	}

	/**
	 * Constructor of coupon.
	 * @param couponColour is color of coupon.
	 */
	public Coupon(String couponColour){
		super(CouponColor.valueOf(couponColour.toUpperCase()).value);
		this.color = couponColour;
	}
	/**
	 * Get color of coupon.
	 * @return color of this coupon.
	 */
	public String getColor(){
		return color;
	}

	/**
	 * Describe coupon.
	 * @return String of coupon.
	 */
	public String toString(){
		return color + " coupon";
	}



}
