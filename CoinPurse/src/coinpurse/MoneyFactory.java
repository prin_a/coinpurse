package coinpurse;

import java.util.ResourceBundle;
/**
 * MoneyFactory that create instance of money factory class.
 * @author Prin Angkunanuwat
 *
 */
public abstract class MoneyFactory {
	private static ResourceBundle bundle = ResourceBundle.getBundle("purse");
	private static MoneyFactory instance;
	/**
	 * Get instance of moneyfactory.
	 * @return instance of Moneyfactory upon purse.property.
	 */
	public static MoneyFactory getInstance(){
		String factory = bundle.getString("MoneyFactory");
		try {
			instance = (MoneyFactory)Class.forName(factory).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return instance;
	}
	/**
	 * Set current factory to new factory.
	 * @param newFactory to be set.
	 */
	public static void setMoneyFactory(MoneyFactory newFactory){
		instance = newFactory;
	}
	/**
	 * Create valuable with value.
	 * @param value of coin or bank.
	 * @return Valuable that has value and currency.
	 */
	public abstract Valuable createMoney( double value );

	/**
	 * Create valuable with value from string.
	 * @param value is string to be set to value.
	 * @return Valuable that has value and currency.
	 */
	public Valuable createMoney(String value){
		return createMoney(Double.parseDouble(value));

	}

}
