package coinpurse;

import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Prin Angkunanuwat
 */
public class Main {
	/**
	 * @param args not used
	 */
	public static void main( String[] args ) {

		RecursiveWithdraw recurWithdraw = new RecursiveWithdraw();
		PurseBalanceObserver balanceObs = new PurseBalanceObserver( );
		PurseStatusObserver statusObs = new PurseStatusObserver();

		// 1. create a Purse
		Purse purse = new Purse(10);
		purse.addObserver(balanceObs);
		purse.addObserver(statusObs);
		purse.setWithdrawStrategy(recurWithdraw);
		// 2. create a ConsoleDialog with a reference to the Purse object
		ConsoleDialog ui = new ConsoleDialog( purse );
		// 3. run() the ConsoleDialog
		ui.run();

	}
}
