package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * Observer for showing balance.It updates when the balance is changed.
 * @author Prin Angkunanuwat
 *
 */
public class PurseBalanceObserver extends JFrame implements Observer {
	
	private JLabel balance;
	/**
	 * Set title and call initComponent.
	 */
	public PurseBalanceObserver(){
 		super("Purse Balance Observer");
		
		this.initComponents();
	}
	/**
	 * Create component used in this frame.
	 */
	private void initComponents() {
		JPanel pane = new JPanel();
		balance = new JLabel("Nothing in the Purse.");
		pane.add(balance);
		super.add(pane);
		super.pack();
		super.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	/**
	 * Whenever purse's content is changed, this gui will change its value.
	 * @param subject an Observable that is changed.
	 * @param info is not used.
	 */
	@Override
	public void update(Observable subject, Object info) {
		// TODO Auto-generated method stub
		if (subject instanceof Purse) {
			Purse purse = (Purse) subject;
			int balance = (int) purse.getBalance();
			this.balance.setText(balance + " Baht");
			super.pack();
			System.out.println("Balance is: " + balance);
		}
		if (info != null) System.out.println( info );

		
	}

	
	
	
	
}
