package coinpurse;

import java.util.Arrays;
import java.util.List;
/**
 * Thaifactory for currency in thailand.
 * @author Prin Angkunanuwat
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{
	List<Double> coins;
	List<Double> banknotes;
	/**
	 * Create array of value for coins and banks.
	 */
	public ThaiMoneyFactory(){
		coins = Arrays.asList(1.0,2.0,5.0,10.0);
		banknotes = Arrays.asList(20.0,50.0,100.0,500.0,1000.0);
	}
	/**
	 * Create valuable with value.
	 * @param value of coin or bank.
	 * @return Valuable that has value and currency.
	 */
	@Override
	public Valuable createMoney(double value) {
		if (coins.contains(value)) 
			return new Coin(value, "Baht");
		if ( banknotes.contains(value))
			return new BankNote(value, "Baht");
		throw new java.lang.IllegalArgumentException();
	}
}
