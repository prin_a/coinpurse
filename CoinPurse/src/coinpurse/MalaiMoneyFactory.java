package coinpurse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * Malaifactory for currency in malaysia.
 * @author Prin Angkuanauwat
 *
 */
public class MalaiMoneyFactory extends MoneyFactory{
	List<Double> coins;
	List<Double> banknotes;
	/**
	 * Create array of value for coins and banks.
	 */
	public MalaiMoneyFactory(){
		coins = Arrays.asList(0.05,0.1,0.2,0.5);
		banknotes = Arrays.asList(1.0,2.0,5.0,10.0,20.0,50.0,100.0);
	}
	/**
	 * Create valuable with value.
	 * @param value of coin or bank.
	 * @return Valuable that has value and currency.
	 */
	@Override
	public Valuable createMoney(double value) {
		if (coins.contains(value)) 
			return new Coin(value*100, "Sen");
		if ( banknotes.contains(value))
			return new BankNote(value, "Ringgit");
		throw new java.lang.IllegalArgumentException();
	}
}
