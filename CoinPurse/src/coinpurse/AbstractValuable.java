package coinpurse;
/**
 * An abstract class that is a superclass of coin, banknote and coupon.
 * Has three method that use in subclass.
 * @author Prin Angkunanuwat
 *
 */
public abstract class AbstractValuable implements Valuable {
	
	private double value;
	private String currency;
	/**
	 * 
	 * @param value of Valuable
	 */
	public AbstractValuable(double value, String currency) {
		super();
		this.value = value;
		this.currency = currency;
	}

	/**
	 * Check is it has the same value. 
	 * @param other is another Object to check is it equal.
	 * @return true if other is not null, other is a Valuable,and other has same value as this Valuable.
	 *
	 */
	public boolean equals(Object other) {
		if( other == null ) return false;
		if( other.getClass() != this.getClass()) return false;
		if( ((Valuable) other).getValue() == this.getValue() ) return true;
		return false;
		
	}
	
	/**
     * Compare the value of the Valuable.
     * @param other is another Valuable to compare with.
     * @return return 1 if this Valuable has higher value than other,0 if the value are same,-1 if this Valuable has lower value than other.
     */
    public int compareTo(Valuable other){
    	if( this.getValue() - other.getValue() < 0 ) return -1;    	
    	if( this.getValue() - other.getValue() == 0 ) return 0;
    	return 1;
    }
    
    /**
	 * Get a value of the Valuable.
	 * @return value of the Valuable.
	 */
	public double getValue(){
		return this.value;
	}
	
	public String getCurrency(){
		return this.currency;
	}


}