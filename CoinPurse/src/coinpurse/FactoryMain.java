package coinpurse;
/**
 * Test for MoneyFactory.
 * @author Prin Angkunanuwat
 *
 */
public class FactoryMain {
	/**
	 * Test for MoneyFactory.
	 * @param args is not used.
	 */
	public static void main(String[] args){
		MoneyFactory factory = MoneyFactory.getInstance();
		System.out.println(factory.createMoney("10"));
		System.out.println(factory.createMoney(50.0));
		Valuable m = factory.createMoney( 5 );
		System.out.println(m.toString());
		Valuable m2 = factory.createMoney("1000.0");
		System.out.println(m2.toString());
		Valuable m3 = factory.createMoney(0.05);
		System.out.println(m3.toString());
	}
}
