package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Prin Angkunanuwat
 */
public class Coin extends AbstractValuable {

    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value , String currency) {
        super(value , currency);
    }

        
    /**
     * Describe coin.
     * @return return the describe of this object.
     */
    public String toString(){
    	return String.format("%.0f %s coin", this.getValue(), this.getCurrency());
    }
    
}

